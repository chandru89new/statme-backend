module.exports = {
  customResponse: (status, msg, data) => {
    if (!status) status = 500;
    if (!msg) msg = "OK";
    return {
      status: status,
      msg: msg,
      data: data
    };
  },
  getParamMaxCount: level => {
    let count;
    switch (level) {
      default:
        count = 10;
        break;
      case 1:
        count = 10;
        break;
      case 2:
        count = 50;
        break;
    }
    return count;
  },
  isValidDate: date => {
    if (!date) return false;
    return new Date(date) == "Invalid Date" ? false : true;
  }
};
