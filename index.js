const express = require("express");
const port = 8000;
const userRoutes = require("./routes/user.js");
const paramRoutes = require("./routes/param.js");
const statRoutes = require("./routes/stat.js");
const confirmRoutes = require("./routes/confirm.js");
app = express();
app.use("/user", userRoutes);
app.use("/param", paramRoutes);
app.use("/stats", statRoutes);
app.use("/confirm", confirmRoutes);

app.get("*", (req, res, next) => {
  res.status(200).send("All OK");
});
app.post("*", (req, res, next) => {
  res.status(200).send("OKK");
});
app.listen(port, () => {
  console.log(`Server live on ${port}`);
});
