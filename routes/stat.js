const express = require("express");
const bodyparser = require("body-parser");
const sequelize = require("sequelize");
const cres = require("../helpers/").customResponse;
const isValidDate = require("../helpers/").isValidDate;
const routes = express.Router();
const models = require("../models");
const User = require("../models").User;
const Param = require("../models").Param;
const Stat = require("../models").Stat;

routes.use(bodyparser.json());

const addStats = (req, res, next) => {
  Stat.bulkCreate(req.body.d)
    .then(result => {
      return res.send(cres(200, `Stats added!`));
    })
    .catch(err => {
      return res.send(cres(500, err.parent.sqlMessage));
    });
};

const routerGuard = async (req, res, next) => {
  /* 
    1. get userId, level, createdAt and isConfirmed
    2. if isConfirmed is 0, throw error
    3. if level = 0 and createdAt + 30 is less than today, throw error
    4. else, proceed by setting userId and level
  */
  /* 
    1. get userId and level using apiKey
    2. if level = 0, check (today <= createdAt + 14)
      - if true, proceed
      - else throw upgrade account msg
  */
  if (!req.headers.authorization) {
    return res.send(cres(401, "No authorization token sent."));
  }
  const apiKey = req.headers.authorization.split(" ")[1];
  const result = await User.find({
    where: {
      apiKey: apiKey
    },
    attributes: ["id", "level", "isConfirmed", "createdAt"]
  }).catch(err => {
    return res.send(cres(500, err.parent.sqlMessage));
  });
  if (!result) {
    return res.send(cres(404, "Invalid token."));
  }
  if (result.isConfirmed == 0) {
    return res.send(
      cres(
        401,
        "Account hasnt been confirmed yet. Check your email for the confirmation link."
      )
    );
  } else {
    const expiryDateInSeconds = result.createdAt.setDate(
      result.createdAt.getDate() + 30
    );
    const expiryDate = new Date(expiryDateInSeconds);
    const today = new Date();
    const level = result.level;
    if (level < 1 && today >= expiryDate) {
      return res.send(cres(401, "Your trial has expired. Please upgrade."));
    } else {
      req.body.userId = result.id;
      req.body.level = result.level;
      next();
    }
  }
};

routes.post("/date", routerGuard, async (req, res, next) => {
  /* 
    1. if no parameter in request, set req.body.date to today and fetch data and show results
    2. if date present, valide. if error, throw error. if not, fetch and show result.
    3. if startDate and endDate present, validate both. if error, throw error. if not, fetch and show result.
  */

  if (!req.body.startDate && !req.body.endDate) {
    if (req.body.date) {
      if (!isValidDate(req.body.date)) {
        return res.send(cres(400, "Wrong date format."));
      }
    } else {
      const todate = new Date();
      req.body.date = todate.toISOString().split("T")[0];
    }
    const stats = await models.sequelize
      .query(
        "select Stats.paramId, Params.param, Stats.value from Params, Stats where Stats.date = ? and Stats.paramId in (select id from Params where userId = ?) and (Params.id = Stats.paramId)",
        {
          replacements: [req.body.date, req.body.userId],
          type: sequelize.QueryTypes.SELECT
        }
      )
      .catch(err => {
        return res.send(cres(500, err.parent.sqlMessage));
      });
    return res.send(cres(200, "OK", { date: req.body.date, stats: stats }));
  }

  if (req.body.startDate || req.body.endDate) {
    if (!isValidDate(req.body.startDate) || !isValidDate(req.body.endDate)) {
      return res.send(
        cres(
          400,
          "Both startDate and endDate need to be given and should be a valid date format."
        )
      );
    }
    const stats = await models.sequelize
      .query(
        "select Stats.date, Stats.paramId, Params.param, Stats.value from Params, Stats where Stats.date between ? and ? and Stats.paramId in (select id from Params where userId = ?) and (Params.id = Stats.paramId)",
        {
          replacements: [req.body.startDate, req.body.endDate, req.body.userId],
          type: sequelize.QueryTypes.SELECT
        }
      )
      .catch(err => {
        return res.send(cres(500, err.parent.sqlMessage));
      });
    return res.send(
      cres(200, "OK", {
        startDate: req.body.startDate,
        endDate: req.body.endDate,
        stats: stats
      })
    );
  }
});

routes.post("/:paramId", routerGuard, async (req, res, next) => {
  /* 
    1. if paramId is zero or not an integer, throw error
    2. if no startDate and endDate, check for date. if no date, set current date and fetch result
    3. if startDate or endDate, validate them and fetch result
  */
  const paramId = req.params.paramId;
  if (paramId == 0 || isNaN(parseInt(paramId))) {
    return res.send(cres(400, "Parameter ID can't be zero."));
  }

  if (!req.body.startDate && !req.body.endDate) {
    if (!req.body.date) {
      req.body.date = new Date().toISOString().split("T")[0];
    }
    if (!isValidDate(req.body.date)) {
      return res.send(cres(400, "Invalid date format."));
    }
    const result = await models.sequelize
      .query(
        "select Stats.value, Stats.paramId, Params.param from Stats, Params where Stats.date = ? and Stats.paramId = ? and Params.id = Stats.paramId and Params.userId = ? LIMIT 1",
        {
          type: sequelize.QueryTypes.SELECT,
          replacements: [req.body.date, req.params.paramId, req.body.userId],
          raw: true
        }
      )
      .catch(err => res.send(cres(500, err.parent.sqlMessage)));
    if (result.length) {
      return res.send(
        cres(200, "OK", {
          date: req.body.date,
          paramId: result[0].paramId,
          value: result[0].value
        })
      );
    } else {
      return res.send(
        cres(404, "No stats found for that parameter on that date.")
      );
    }
  } else {
    if (!isValidDate(req.body.startDate) || !isValidDate(req.body.endDate)) {
      return res.send(
        cres(
          400,
          "startDate and endDate should be in a valid date format for this to work (and if you give one and skip the other, you'll see this error)."
        )
      );
    }
    const result = await models.sequelize.query(
      "select Stats.date, Params.param, Stats.value from Stats, Params where Stats.paramId = ? and Stats.date between ? and ? and Params.userId = ? and Params.id = Stats.paramId",
      {
        replacements: [
          req.params.paramId,
          req.body.startDate,
          req.body.endDate,
          req.body.userId
        ],
        type: sequelize.QueryTypes.SELECT,
        raw: true
      }
    );
    if (result.length > 0) {
      const formattedResult = result.map(item => {
        return {
          ...item,
          date: item.date.toISOString().split("T")[0]
        };
      });
      return res.send(cres(200, "OK", formattedResult));
    } else {
      return res.send(
        cres(404, "Nothing found for that date range and parameter.")
      );
    }
  }
});

routes.post("/", routerGuard, async function(req, res, next) {
  /*  
    
    1. if req has no date, throw error
    2. if req.body.stats object is empty, throw error
    3. if already added for this date, throw error
    4. else, foreach key in the stats do this:
      - verify if there is a record in Params table "where userId = req.body.userId and param = req.body.stats[param]" and get the id of that record
      - if there is a valid id, then use that to execute .create in stats (insert into with paramId, value, date)
    5. execute a final query on Stats (where date = req.body.date) and res.send the result
  */

  if (!req.body.date) {
    return res.send(cres(400, "No date found in the request."));
  }

  if (!req.body.stats || Object.keys(req.body.stats).length == 0) {
    return res.send(cres(400, "No stats to add."));
  }

  const alreadyAdded = await models.sequelize
    .query(
      "SELECT id FROM Stats WHERE date = ? AND paramId IN (SELECT id from Params WHERE userId = ?)",
      {
        replacements: [req.body.date, req.body.userId],
        type: sequelize.QueryTypes.SELECT
      }
    )
    .catch(err => {
      return res.send(cres(500, err.parent.sqlMessage));
    });

  if (alreadyAdded.length) {
    return res.send(cres(400, "You've already added stats for that date."));
  }

  const params = Object.keys(req.body.stats);
  const paramObjectPromises = params.map(param => {
    return Param.find({
      where: { userId: req.body.userId, param: param },
      attributes: ["id", "param"]
    });
  });
  Promise.all(paramObjectPromises)
    .then(result => {
      if (result.length) {
        let d = [];
        result.forEach(item => {
          if (!item) return;
          else {
            return d.push({
              paramId: item.id,
              value: req.body.stats[item.param],
              date: new Date(req.body.date)
            });
          }
        });
        req.body.d = d;
        addStats(req, res);
      }
    })
    .catch(err => {
      return res.send(cres(500, err.parent.sqlMessage));
    });
});

routes.delete("/", routerGuard, async function(req, res, next) {
  /* 
    1. if no date given, throw error
    2. if date, fetch stats for that date and delete. if no result, show "No stats present."
  */

  if (!req.body.date || !isValidDate(req.body.date)) {
    return res.send(cres(400, "No date given or date format is invalid."));
  }

  const result = await models.sequelize
    .query(
      "delete from Stats where Stats.date = ? and Stats.paramId in (select id from Params where userId = ?)",
      {
        type: sequelize.QueryTypes.DELETE,
        raw: true,
        replacements: [req.body.date, req.body.userId]
      }
    )
    .catch(err =>
      res.send(cres(500, "Couldnt delete stats due to some database issue."))
    );
  return res.send(cres(200, "Stats deleted.", result));
});

module.exports = routes;
