const express = require("express");
const bodyparser = require("body-parser");
const cres = require("../helpers").customResponse;
const getParamMaxCount = require("../helpers").getParamMaxCount;
const routes = express.Router();
const models = require("../models");
const User = require("../models").User;
const Param = require("../models").Param;
const Stat = require("../models").Stat;

routes.use(bodyparser.json());

const addParam = (req, res) => {
  Param.create({
    userId: req.body.userId,
    param: req.body.param
  })
    .then(result => {
      return res.send(cres(200, "Parameter added.", { id: result.id }));
    })
    .catch(err => {
      return res.send(cres(500, err.parent.sqlMessage));
    });
};
const deleteParam = async (req, res) => {
  // await Param.destroy({
  //   where: { id: req.body.id, userId: req.body.userId }
  // });
  // await Stat.destroy({
  //   where: { paramId: req.body.id }
  // });
  // return res.send(cres(200, "Deleted that parameter."));
  return models.sequelize
    .transaction(t => {
      return Param.destroy(
        {
          where: { id: req.body.id, userId: req.body.userId }
        },
        { transaction: t }
      ).then(r => {
        return Stat.destroy(
          {
            where: { paramId: req.body.id }
          },
          { transaction: t }
        );
      });
    })
    .then(r => res.send(cres(200, "Parameter deleted.")))
    .catch(err => res.send(cres(500, err.parent.sqlMessage)));
};
const updateParam = (req, res) => {
  Param.update(
    {
      param: req.body.param
    },
    {
      where: {
        id: req.body.id
      }
    }
  )
    .then(result => {
      return res.send(cres(200, "Parameter updated."));
    })
    .catch(err => {
      return res.send(cres(500, err.parent.sqlMessage));
    });
};

const routerGuard = async (req, res, next) => {
  /* 
    1. get userId, level, createdAt and isConfirmed
    2. if isConfirmed is 0, throw error
    3. if level = 0 and createdAt + 30 is less than today, throw error
    4. else, proceed by setting userId and level
  */
  /* 
    1. get userId and level using apiKey
    2. if level = 0, check (today <= createdAt + 14)
      - if true, proceed
      - else throw upgrade account msg
  */
  if (!req.headers.authorization) {
    return res.send(cres(401, "No authorization token sent."));
  }
  const apiKey = req.headers.authorization.split(" ")[1];
  const result = await User.find({
    where: {
      apiKey: apiKey
    },
    attributes: ["id", "level", "isConfirmed", "createdAt"]
  }).catch(err => {
    return res.send(cres(500, err.parent.sqlMessage));
  });
  if (!result) {
    return res.send(cres(401, "Invalid token."));
  }
  if (result.isConfirmed == 0) {
    return res.send(
      cres(
        401,
        "Account hasnt been confirmed yet. Check your email for the confirmation link."
      )
    );
  } else {
    const expiryDateInSeconds = result.createdAt.setDate(
      result.createdAt.getDate() + 30
    );
    const expiryDate = new Date(expiryDateInSeconds);
    const today = new Date();
    const level = result.level;
    if (level < 1 && today >= expiryDate) {
      return res.send(cres(401, "Your trial has expired. Please upgrade."));
    } else {
      req.body.userId = result.id;
      req.body.level = result.level;
      next();
    }
  }
};

routes.get("/", routerGuard, (req, res, next) => {
  Param.findAll({
    where: {
      userId: req.body.userId
    },
    attributes: ["id", "param"]
  })
    .then(result => {
      if (!result.length) return res.send(cres(404, "No params found"));
      res.send(cres(200, "All good.", result));
    })
    .catch(err => {
      res.send(cres(500, err.parent.sqlMessage));
    });
});

routes.post("/", routerGuard, (req, res, next) => {
  /*
    1. throw error if there is no req.body.param OR if it is not a string
    2. get paramMaxCount from req.body.level
    3. get count of all params for this req.body.userId
    4. if count >= paramMaxCount, throw error
    5. else check for param duplicate and throw error if duplicate
    6. add parameter
  */

  if (
    !req.body.param ||
    req.body.param.length <= 0 ||
    !isNaN(parseInt(req.body.param))
  ) {
    return res.send(
      cres(
        400,
        "No parameter to add (or you sent some gibberish instead of the parameter)."
      )
    );
  }

  const paramMaxCount = getParamMaxCount(req.body.level);

  Param.count({ where: { userId: req.body.userId } })
    .then(count => {
      if (count >= paramMaxCount) {
        return res.send(
          cres(401, "You need to upgrade your account to add more parameters.")
        );
      } else {
        Param.find({
          where: {
            param: req.body.param,
            userId: req.body.userId
          }
        })
          .then(result => {
            if (result) {
              return res.send(
                cres(400, "You already have a parameter by that name.")
              );
            } else {
              addParam(req, res);
            }
          })
          .catch(err => {
            return res.send(cres(500, err.parent.sqlMessage));
          });
      }
    })
    .catch(err => {
      res.send(cres(500, err.parent.sqlMessage));
    });
});

routes.put("/", routerGuard, function(req, res, next) {
  /*  
    1. if no param id and param name, throw error
    2. get record for userId and id. if no record, throw error.
    3. if record exists, check if duplicate. if yes, throw error.
    4. update 
    5. update in Stats table too. TODO
  */

  if (!req.body.id || !req.body.param) {
    return res.send(
      cres(400, "Need an id and a new parameter name to update.")
    );
  }
  Param.find({
    where: {
      id: req.body.id,
      userId: req.body.userId
    }
  })
    .then(result => {
      // res.send(cres(200, "OK", result));
      if (!result) {
        return res.send(cres(400, "No parameter found with that id."));
      } else {
        // updateParam(req, res);
        Param.find({
          where: {
            userId: req.body.userId,
            param: req.body.param
          },
          attributes: ["id"],
          raw: true
        })
          .then(result => {
            if (result != null) {
              return res.send(
                cres(400, "You already have a parameter by that name.")
              );
            } else {
              updateParam(req, res);
            }
          })
          .catch(err => res.send(cres(500, err.parent.sqlMessage)));
      }
    })
    .catch(err => {
      return res.send(cres(500, err.parent.sqlMessage));
    });
});

routes.delete("/", routerGuard, (req, res, next) => {
  /* 
    1. if no id, throw error
    2. check if id, userId returns a record. if not, throw error
    3. delete the record
    4. delete all stats for that paramId TODO
  */
  if (!req.body.id) {
    return res.send(
      cres(400, "Nothing to delete because no id in the request.")
    );
  }

  Param.find({
    where: {
      id: req.body.id,
      userId: req.body.userId
    }
  })
    .then(result => {
      if (!result) {
        return res.send(cres(404, "No such parameter id found."));
      } else {
        deleteParam(req, res);
      }
    })
    .catch(err => {
      return res.send(cres(500, err.parent.sqlMessage));
    });
});

module.exports = routes;
