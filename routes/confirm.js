/* 
  This confirms the account.
  1. Input: query param (q)
  2. Process:
    - decodes the query param
    - splits into email and confirmationToken
    - queries Users table (for email + confirmationToken). 
      - if no row returned, throws error output
      if isConfirmed = 1, throws error output (already confirmed)
      - if confirmationExpiry is older than NOW:
        - reset confirmationExpiry to today + 1
        - get a new confirmationToken
        - Update Users table with new expiry and token
        - resend confirmation email
        - output about all this
    - sets isConfirmed to 1
    - output: msg: "Email confirmed. Login."
*/

const express = require("express");
// const bodyparser = require("body-parser");
const cres = require("../helpers").customResponse;
const atob = require("atob");
const routes = express.Router();
const User = require("../models/").User;
const sendConfirmationEmail = require("../sendconfirmemail");
const ID = require("uneeqid");
ID.length = 64;
routes.get("/", async (req, res, next) => {
  if (!req.query.q) {
    return res.send("Invalid request");
  }
  const q = req.query.q;
  const email = atob(q).split(":")[0];
  const confirmationToken = atob(q).split(":")[1];
  const row = await User.find({
    where: { email: email, confirmationToken: confirmationToken },
    attributes: ["isConfirmed", "confirmationExpiry", "id"]
  }).catch(err =>
    res.send(
      "Invalid request. Query exec failure. Please contact me => (hi@stat.kim)"
    )
  );
  if (!row) {
    return res.send(
      "Invalid confirmation token. Please contact me => hi@stat.kim"
    );
  }
  if (row.isConfirmed == 1) {
    return res.send(
      "Account is already confirmed. If you have problems logging in, please contact me => hi@stat.kim"
    );
  }
  const today = new Date();
  if (row.confirmationExpiry < today) {
    const newConfirmationExpiry = new Date(
      new Date().setDate(new Date().getDate() + 1)
    );
    const newConfirmationToken = ID.generate();
    User.update(
      {
        confirmationExpiry: newConfirmationExpiry,
        confirmationToken: newConfirmationToken
      },
      {
        where: {
          email: email
        }
      }
    )
      .then(result => {
        sendConfirmationEmail(email, newConfirmationToken)
          .then(result => {
            return res.send(
              "Confirmation link has expired. Sent you a new one. Please use that."
            );
          })
          .catch(err => {
            return res.send(
              "Confirmation link has expired but I was unable to send you another link. Please contact me => hi@stat.kim <= to activate your account."
            );
          });
      })
      .catch(err => {
        return res.send(
          "Confirmation link has expired but I was unable to set another one. Please contact me => hi@stat.kim"
        );
      });
    return;
  }
  User.update({ isConfirmed: 1 }, { where: { email: email } })
    .then(result =>
      res.send("Account confirmed! You can start using Stat.kim (via the API).")
    )
    .catch(err =>
      res.send(
        "Could not confirm your account. Please contact me => hi@stat.kim"
      )
    );
});

module.exports = routes;
