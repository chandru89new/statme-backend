const bcrypt = require("bcrypt");
const ID = require("uneeqid");
const express = require("express");
const bodyparser = require("body-parser");
const cres = require("../helpers").customResponse;
const atob = require("atob");
const sendConfirmationEmail = require("../sendconfirmemail");
const routes = express.Router();
const User = require("../models").User;
ID.type = "alphanumeric";

routes.use(bodyparser.json());

// const routerGuard = (req, res, next) => {
//   console.log("okay");
//   next();
// };

routes.get("/", (req, res, next) => {
  const r = cres(200, "OK");
  return res.send(r);
});

routes.post("/login", (req, res, next) => {
  const basicAuth = req.headers.authorization;
  const base64Data =
    basicAuth && basicAuth.indexOf("Basic") >= 0
      ? basicAuth.split(" ")[1]
      : null;
  if (base64Data) {
    const email = atob(base64Data).split(":")[0];
    const pass = atob(base64Data).split(":")[1];
    User.find({
      where: {
        email: email
      },
      attributes: ["phash"]
    })
      .then(result => {
        if (!result) {
          const r = cres(404, "Username not found.");
          return res.send(r);
        }
        const pwdIsValid = bcrypt.compareSync(pass, result.phash);
        if (!pwdIsValid) {
          const r = cres(401, "Username/password not valid.");
          return res.send(r);
        } else {
          User.find({
            where: {
              email: email
            },
            attributes: ["email", "apiKey", "level", "isConfirmed"]
          })
            .then(result => {
              const r = cres(200, "OK", result);
              return res.send(r);
            })
            .catch(err => {
              const r = cres(500, "Internal server error.");
              return res.send(r);
            });
        }
      })
      .catch(err => {
        const r = cres(500, err.parent.sqlMessage);
        return res.status(r);
      });
  }
});

routes.post("/register", (req, res, next) => {
  const base = req.headers.authorization.split(" ")[1];
  const email = atob(base).split(":")[0];
  const pwd = atob(base).split(":")[1];
  // email and pass validations here
  if (!pwd || pwd.length < 6) {
    const r = cres(401, "Password can't be empty or less than 6 chars long");
    return res.send(r);
  }
  /* 
    1. generate apiKey, confirmation_token and confirmation_expiry
    2. User.create with these values
    3. If success, send an email to the given email
    4. Return success message
  */
  ID.length = 128;
  const apiKey = ID.generate();
  ID.length = 64;
  const confirmationToken = ID.generate();
  const confirmationExpiry = new Date(
    new Date().setDate(new Date().getDate() + 1)
  );
  User.create(
    {
      email: email,
      phash: pwd,
      apiKey: apiKey,
      confirmationToken: confirmationToken,
      confirmationExpiry: new Date(new Date().setDate(new Date().getDate() + 1))
    },
    {
      fields: [
        "email",
        "phash",
        "apiKey",
        "confirmationToken",
        "confirmationExpiry"
      ]
    }
  )
    .then(result => {
      // send email
      sendConfirmationEmail(email, confirmationToken)
        .then(result =>
          res.send(cres(200, "User created and confirmation email sent."))
        )
        .catch(err =>
          res.send(
            cres(
              200,
              "User created but confirmation email not sent (or status unknown). Please reach out to me => hi@stat.kim"
            )
          )
        );
    })
    .catch(err => res.send(cres(500, err.parent.sqlMessage)));
});

module.exports = routes;
