"use strict";
module.exports = (sequelize, DataTypes) => {
  const Stat = sequelize.define(
    "Stat",
    {
      id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true
      },
      paramId: { type: DataTypes.INTEGER, allowNull: false },
      value: {
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 0
      },
      date: {
        type: DataTypes.DATEONLY,
        allowNull: false
      },
      createdAt: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: new Date()
      },
      updatedAt: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: new Date()
      }
    },
    {}
  );
  Stat.associate = function(models) {
    // associations can be defined here
  };
  return Stat;
};
