"use strict";
module.exports = (sequelize, DataTypes) => {
  const Param = sequelize.define(
    "Param",
    {
      userId: { type: DataTypes.INTEGER, allowNull: false },
      param: { type: DataTypes.STRING, allowNull: false },
      createdAt: { type: DataTypes.DATE, defaultValue: new Date() },
      updatedAt: { type: DataTypes.DATE, defaultValue: new Date() }
    },
    {}
  );
  Param.associate = function(models) {
    // associations can be defined here
  };
  return Param;
};
