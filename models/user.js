"use strict";
const bc = require("bcrypt");
module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define(
    "User",
    {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        unique: true
      },
      email: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true,
        validate: { isEmail: true }
      },
      phash: {
        type: DataTypes.STRING,
        allowNull: false,
        set(val) {
          this.setDataValue("phash", bc.hashSync(val, 10));
        },
        scopes: false
      },
      apiKey: {
        type: DataTypes.STRING,
        validate: { len: [12, 128] },
        allowNull: false
      },
      level: { type: DataTypes.INTEGER },
      isConfirmed: {
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 0
      },
      confirmationToken: { type: DataTypes.STRING, allowNull: false },
      confirmationExpiry: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: new Date(new Date().setDate(new Date().getDate() + 1))
      }
    },
    {
      hooks: {
        afterCreate: r => {
          delete r.dataValues.phash;
          delete r.dataValues.createdAt;
          delete r.dataValues.updatedAt;
          return r;
        }
      }
    }
  );

  return User;
};
