const aws = require("aws-sdk");
const btoa = require("btoa");
const env = process.env.NODE_ENV || "development";
const fromEmail = require("./config/global")[env].fromEmail; // global config
const apiRoot = require("./config/global")[env].confirmEmailEndpoint; // global config;
aws.config.loadFromPath("./config/aws.json");

const sendConfirmationEmail = (email, key) => {
  const queryParam = btoa(email + ":" + key);
  const ses = new aws.SES();
  const dataHTML = `You (or someone using your email) signed up for <a href="https://stat.kim/">stat.kim</a>. If that was indeed you, please <a href="${apiRoot}/?q=${queryParam}">click here to confirm</a> and your account will be activated.`;
  const dataTXT = `You (or someone using your email) signed up for stat.kim. If that was indeed you, please go to this link (https://${apiRoot}/?q=${queryParam}) to confirm and your account will be activated.`;
  return new Promise((resolve, reject) => {
    const params = {
      Destination: { ToAddresses: [email] },
      Message: {
        Body: {
          Html: { Charset: "UTF-8", Data: dataHTML },
          Text: { Charset: "UTF-8", Data: dataTXT }
        },
        Subject: { Charset: "UTF-8", Data: "Confirm your Stat.kim account" }
      },
      ReturnPath: fromEmail,
      Source: fromEmail
    };

    ses.sendEmail(params, (err, data) => {
      if (err) {
        reject(err);
      } else {
        resolve(data);
      }
    });

    setTimeout(function() {
      reject(1);
    }, 10000);
  });
};
module.exports = sendConfirmationEmail;
