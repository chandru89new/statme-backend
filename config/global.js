module.exports = {
  development: {
    fromEmail: "Chandru from Statkim <hi@stat.kim>",
    apiRoot: "http://localhost:8000",
    confirmEmailEndpoint: "http://localhost:8000/confirm"
  },
  production: {
    fromEmail: "Chandru from Statkim <hi@stat.kim>",
    apiRoot: "https://api.stat.kim",
    confirmEmailEndpoint: "https://api.stat.kim/confirm"
  }
};
